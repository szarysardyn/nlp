#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 30 15:18:20 2019

@author: szary
"""

import tensorflow as tf
tf.enable_eager_execution()
import tensorflow_datasets as tfds
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.regularizers import l2
import matplotlib.pyplot as plt
import numpy as np
import io


#load the data from keras datasets
def load_data():
    val_data, test_data = tfds.Split.TEST.subsplit(weighted=[1, 4])
    train_data = tfds.load('imdb_reviews', split = 'train', as_supervised = True)
    val_data = tfds.load('imdb_reviews', split = val_data, as_supervised = True)
    test_data = tfds.load('imdb_reviews', split = test_data, as_supervised = True)
    return train_data, val_data, test_data

#extract sentences and labels from tf.Tensor
def prepare_datasets(train_data, val_data, test_data):
    training_sentences = []
    training_labels = []

    testing_sentences = []
    testing_labels = []

    validation_sentences = []
    validation_labels = []
    
    for s, l in train_data:
        training_sentences.append(str(s.numpy()))
        training_labels.append(str(l.numpy()))

    for s, l in val_data:
        validation_sentences.append(str(s.numpy()))
        validation_labels.append(str(l.numpy()))
    
    for s, l in test_data:
        testing_sentences.append(str(s.numpy()))
        testing_labels.append(str(l.numpy()))
            
    training_labels = np.array(training_labels)
    validation_labels = np.array(validation_labels)
    testing_labels = np.array(testing_labels)
        
    return training_sentences, training_labels, validation_sentences, validation_labels, testing_sentences, testing_labels

#transform sentences to vectors
def text_transform(training_sentences, validation_sentences, testing_sentences, vocab_size, max_length):
    trunc_type = 'post'
    oov_tok = '<OOV>'
    
    tokenizer = Tokenizer(num_words = vocab_size, oov_token = oov_tok)
    tokenizer.fit_on_texts(training_sentences)
    word_index = tokenizer.word_index
    training_sequences = tokenizer.texts_to_sequences(training_sentences)
    training_padded = pad_sequences(training_sequences, maxlen = max_length, truncating = trunc_type)
    
    validation_sequences = tokenizer.texts_to_sequences(validation_sentences)
    validation_padded = pad_sequences(validation_sequences, maxlen = max_length, truncating = trunc_type)
    
    testing_sequences = tokenizer.texts_to_sequences(testing_sentences)
    testing_padded = pad_sequences(testing_sequences, maxlen = max_length, truncating = trunc_type)

    return training_padded, validation_padded, testing_padded, word_index, example

#process the datasets into a final form
def get_datasets(vocab_size, max_length):
    train_data, val_data, test_data = load_data()
    training_sentences, training_labels, validation_sentences, validation_labels, testing_sentences, testing_labels = prepare_datasets(train_data, val_data, test_data)
    training_padded, validation_padded, testing_padded, word_index, example = text_transform(training_sentences, validation_sentences, testing_sentences, vocab_size, max_length)
    
    return training_padded, training_labels, validation_padded, validation_labels, testing_padded, testing_labels, word_index

#define model structure
def get_model(vocab_size, embedding_dim, max_length):
    model = tf.keras.Sequential([
            tf.keras.layers.Embedding(vocab_size, embedding_dim, input_length = max_length),
            tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(128, return_sequences = True)),
            tf.keras.layers.Dropout(0.8),
            tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(64, return_sequences = True)),
            tf.keras.layers.Dropout(0.6),
            tf.keras.layers.GlobalMaxPool1D(),
            tf.keras.layers.Dense(20, activation = 'relu', kernel_regularizer = l2(0.01)),
            tf.keras.layers.Dense(1, activation = 'sigmoid')
            ])
    
    model.compile(loss = 'binary_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
    
    return model

#train the model
def train(model, num_epochs, training_padded, training_labels, validation_data):
    history = model.fit(training_padded,
                        training_labels,
                        epochs = num_epochs,
                        validation_data = validation_data)
    
    return history

#plot the results: accuracy and loss function over epochs  
def plot(history, string):
    plt.plot(history.history[string])
    plt.plot(history.history['val_' + string])
    plt.xlabel('Epochs')
    plt.ylabel(string)
    plt.legend([string, 'val_' + string])
    plt.show()
    
#use the trained weights to visualize the data
def to_visualize(model, word_index, vocab_size):    
    layer = model.layers[0]
    weights = layer.get_weights()[0]
    reverse_word_index = {value: key for key, value in word_index.items()}
    out_v = io.open('vecs.tsv', 'w', encoding = 'utf-8')
    out_m = io.open('meta.tsv', 'w', encoding = 'utf-8')
    for word_num in range(1, vocab_size):
        word = reverse_word_index[word_num]
        embeddings = weights[word_num]
        out_m.write(word + '\n')
        out_v.write('\t'.join([str(x) for x in embeddings]) + '\n')
    out_v.close()
    out_m.close()

def run():
    vocab_size = 10000
    embedding_dim = 128
    max_length = 400
    num_epochs = 3
    
    training_padded, training_labels, validation_padded, validation_labels, testing_padded, testing_labels, word_index = get_datasets(vocab_size, max_length)
    
    model = get_model(vocab_size, embedding_dim, max_length)
    model.summary()

    history = train(model, num_epochs, training_padded, training_labels, (validation_padded, validation_labels))
    
    model.save_weights('sentiment_weights.hdf5')
    
    print(model.evaluate(testing_padded, testing_labels))
    
    to_visualize(model, word_index, vocab_size)
    
    plot(history, 'acc')
    plot(history, 'loss')
    
run()





























