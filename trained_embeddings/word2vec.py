#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 13 13:33:27 2019

@author: szary
"""

import numpy as np
from w2v_utils import *

def cosine_similarity(v, u):
    
    #compute dot product of both vectors (projection of v onto u)
    dot_product = np.dot(v, u)
    #now compute the norms of both vectors 
    v_length = np.linalg.norm(v)
    u_length = np.linalg.norm(u)
    
    #now to compute cosine similarity (range between -1 and 1)
    cosine_similarity = dot_product/(v_length*u_length)
    
    return cosine_similarity

def find_analogy(word_a, word_b, word_c, word_to_vec_map):
    #find vector representations of gven words
    e_a = word_to_vec_map[word_a]
    e_b = word_to_vec_map[word_b]
    e_c = word_to_vec_map[word_c]
    
    #now get the dict keys so we can iterate over them
    words = word_to_vec_map.keys()
    max_cosine_sim = -100
    best_word = None
    
    #iterate over words to find the best candidate
    for word in words:
        if word in [word_a, word_b, word_c]:
            continue
        
        cosine_sim = cosine_similarity(e_b - e_a, word_to_vec_map[word] - e_c)
        
        if cosine_sim > max_cosine_sim:
            max_cosine_sim = cosine_sim
            best_word = word
            
    return best_word
    
words, word_to_vec_map = read_glove_vecs('glove.6B.50d.txt')

analogies = [('italy', 'italian', 'spain'), ('japan', 'japanese', 'poland'), ('man', 'woman', 'boy'), ('small', 'smaller', 'large')]

for analogy in analogies:
    print('{} -> {} :: {} -> {}'.format(*analogy, find_analogy(*analogy, word_to_vec_map)))
