#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  3 13:54:15 2019

@author: szary
"""
import numpy as np
from random import shuffle
import matplotlib.pyplot as plt
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Embedding, LSTM, Dense, Bidirectional
from tensorflow.keras.regularizers import l2
from tensorflow.keras.utils import to_categorical 


def get_data():
    f = open('englishpoetry.txt', 'r')
    sentences = []
    for line in f:
        sentences.append(line)

    sentences = [line.rstrip().lower() for line in sentences if line.strip() != '']
    return np.array(sentences)

def text_transform():
    sentences = get_data()
    shuffle(sentences)
    
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(sentences)
    
    total_words = len(tokenizer.word_index) + 1
    sequences = []
    
    for line in sentences:
        token_list = tokenizer.texts_to_sequences([line])[0]
        for i in range(1, len(token_list)):
            n_gram = token_list[:i+1]
            sequences.append(n_gram)
    
    max_length = max([len(sequence) for sequence in sequences])
    padded_sequences = pad_sequences(sequences, maxlen = max_length, padding = 'pre')

    train_sequences = []
    labels = []
    
    for seq in padded_sequences:
        train_sequences.append(seq[:-1])
        labels.append(seq[-1])
    
    train_sequences = np.array(train_sequences)
    train_labels = to_categorical(labels, num_classes = total_words)
    
    return train_sequences, train_labels, total_words, max_length, tokenizer 

def plot(history, string):
    plt.plot(history.history[string])
    plt.plot(history.history['val_' + string])
    plt.xlabel('Epochs')
    plt.ylabel(string)
    plt.legend([string, 'val_' + string])
    plt.show()

def get_model(total_words, max_length):
    model = Sequential([
            Embedding(total_words, 120, input_length = max_length - 1),
            Bidirectional(LSTM(128, return_sequences = True)),
            Bidirectional(LSTM(64)),
            Dense(total_words*2/3, activation = 'relu', kernel_regularizer = l2(0.01)),
            Dense(total_words, activation = 'softmax')
            ])
    
    model.compile(loss = 'categorical_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
    model.summary()
    
    return model

def generate_poetry(model, seed_text, tokenizer, max_length):
    next_words = 100
    
    for i in range(next_words):
        token_list = tokenizer.texts_to_sequences([seed_text])[0]
        token_list = pad_sequences([token_list], maxlen = max_length, padding='pre')
        predicted = model.predict_classes(token_list, verbose=0)
        output_word = ""
        for word, index in tokenizer.word_index.items():
             if index == predicted:
                 output_word = word
                 seed_text += " " + output_word
                 break
    
    print(seed_text)
    

train_sequences, train_labels, total_words, max_length, tokenizer = text_transform()

model = get_model(total_words, max_length)
#model.load_weights('poetryweights.hdf5')
history = model.fit(train_sequences, train_labels, epochs = 100, verbose = 1)

model.save_weights('poetryweights.hdf5')

plot(history, 'acc')
#plot(history, 'loss')

seed_text = 'the man who stands above'
generate_poetry(model, seed_text, tokenizer, max_length - 1)




