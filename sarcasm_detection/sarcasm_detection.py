#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 31 21:59:40 2019

@author: szary
"""

import json
import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import matplotlib.pyplot as plt

def get_dataset(training_size):
    with open('sarcasm.json', 'r') as f:
        datastore = json.load(f)
    
    sentences = []
    labels = []
    
    for item in datastore:
        sentences.append(item['headline'])
        labels.append(item['is_sarcastic'])
    
    training_sentences = sentences[:training_size]
    training_labels = labels[:training_size]
    
    testing_sentences = sentences[training_size:]
    testing_labels = labels[training_size:]
    
    return training_sentences, training_labels, testing_sentences, testing_labels

def text_transform(training_sentences, testing_sentences, vocab_size, oov_tok, max_length, padding_type, trunc_type):
    tokenizer = Tokenizer(num_words = vocab_size, oov_token = oov_tok)
    tokenizer.fit_on_texts(training_sentences)
    word_index = tokenizer.word_index
    
    training_sequences = tokenizer.texts_to_sequences(training_sentences)
    training_padded = pad_sequences(training_sequences, maxlen = max_length, padding = padding_type, truncating = trunc_type)
    
    testing_sequences = tokenizer.texts_to_sequences(testing_sentences)
    testing_padded = pad_sequences(testing_sequences, maxlen = max_length, padding = padding_type, truncating = trunc_type)
    
    return training_padded, testing_padded

def get_model(vocab_size, embedding_dim, max_length):
    model = tf.keras.Sequential([
            tf.keras.layers.Embedding(vocab_size, embedding_dim, input_length = max_length),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(64)),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.Dense(25, activation = 'relu'),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.Dense(1, activation = 'sigmoid')
            ])
    
    model.compile(loss = 'binary_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
    model.summary()
    
    return model

def train(model, training_padded, training_labels, validation_data, num_epochs):
    history = model.fit(training_padded, training_labels, epochs = num_epochs, validation_data = validation_data, verbose = 1)
    
    return history

def plot(history, string):
    plt.plot(history.history[string])
    plt.plot(history.history['val_' + string])
    plt.xlabel('Epochs')
    plt.ylabel('string')
    plt.legend([string, 'val_' + string])
    plt.show()

def run():
    vocab_size = 10000
    embedding_dim = 20
    max_length = 350
    trunc_type = 'post'
    padding_type = 'post'
    oov_tok = '<OOV>'
    training_size = 20000
    num_epochs = 5

    training_sentences, training_labels, testing_sentences, testing_labels = get_dataset(training_size)
    training_padded, testing_padded = text_transform(training_sentences, testing_sentences, vocab_size, oov_tok, max_length, padding_type, trunc_type)
    
    model = get_model(vocab_size, embedding_dim, max_length)
    
    history = train(model, training_padded, training_labels, (testing_padded, testing_labels), num_epochs)
    
    plot(history, 'acc')
    plot(history, 'loss')
    
run()


























